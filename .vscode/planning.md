## Final Project Planning
--------
==*Instructions*==

**Project pieces:**
- [x] Projects
- [x] Tasks
- [x] ability to login
- [x] ability to logout
- [x] ability to sign up
- [x] 8 html templates (including base.html)
- [x] 7 views
- [x] 2 app-specific models
  - [x] Project model
  - [x] Task model


**Routinely check quality of code:**
`flake8 accounts projects tasks`
`black --check accounts projects tasks`

**Before submitting,properly indent HTML**
`djhtml -i «path to HTML template»`


----------

### Feature 1
- [x] Fork and clone the starter project from Project Alpha 
- [x] Create a new virtual environment in the repository directory for the project: `python -m venv .venv`
- [x] Activate the virtual environment: `source ./.venv/bin/activate`
- [x] Upgrade pip: `python -m pip install --upgrade pip`
- [x] Install django: `pip install django`
- [x] Install black: `pip install black`
- [x] Install flake8: `pip install flake8`
- [x] Install djhtml: `pip install djhtml`
- [x] Use pip freeze to generate a requirements.txt file: `pip freeze > requirements.txt`

**Test Feature 1**
- [x] `python -m unittest tests.test_feature_01`: OK

**Add and commit progress:**
  - [x] `git status`
  - [x] `git add .`
  - [x] `git status`
  - [x] `git commit -m "Feature 1 Complete`
  - [x] `git push origin main`

------------

### Feature 2

**Create Project & Apps**
- [x] Create a Django Project named ==*tracker*==: `django-admin startproject name .`
- [x] Create a Django App named ==*accounts*==: `python manage.py startapp name`
- [x] Install the *accounts* App in the *tracker* Project, settings.py -> INSTALLED_APPS: `"accounts.apps.AccountsConfig"`
- [x] Create a Django App named ==*projects*==: `python manage.py startapp name`
- [x] Install the *projects* App in the *tracker* Project, settings.py -> INSTALLED_APPS: `"projects.apps.ProjectsConfig"`
- [x] Create a Django App named ==*tasks*==: `python manage.py startapp name`
- [x] Install the *tasks* App in the *tracker* Project, settings.py -> INSTALLED_APPS: `"tasks.apps.TasksConfig"`

**Run Migrations**
- [x] `python manage.py makemigrations`
- [x] `python manage.py migrate`

**Create a super user**
- [x] `python manage.py createsuperuser`

**Test Feature 2**
- [x] `python manage.py test tests.test_feature_02`: OK

**Add and commit progress:**
  - [x] `git status`
  - [x] `git add .`
  - [x] `git status`
  - [x] `git commit -m "Feature 2 Complete`
  - [x] `git push origin main`


>**You now have:**
1 Django Project: tracker
3 Django Apps: accounts, projects, tasks


-------------

###Feature 3

>**Working in *projects* App, models.py**

**Import:**
- [x] `from django.contrib.auth.models import User`


**Model**
- [x] Create a ==*Project*== model in the *projects* App: `class Project(models.Model)`
  
*Project* model attributes and methods
- [x] `name = models.CharField(max_length=200)`
- [x] `description = models.TextField()`
- [x] `members = models.ManyToManyField(User, related_name="projects")`
- [x] def __str__ method that is the value of name property:
  - [x] `def __str__(self):`
            `return self.name`

**Run Migrations**
- [x] `python manage.py makemigrations`
- [x] `python manage.py migrate`

**Test Feature 3**
- [x] `python manage.py test tests.test_feature_03`: OK
  
**Add and commit progress:**
  - [x] `git status`
  - [x] `git add .`
  - [x] `git status`
  - [x] `git commit -m "Feature 3 Complete`
  - [x] `git push origin main`

----------------

### Feature 4 ###

>**Working in *projects* App, admin.py**

- [x] Register *Project* model in *projects* App, admin.py
  - [x] import model: `from projects.models import Project`
  - [x] create admin class: `class ProjectAdmin(admin.ModelAdmin): pass`
  - [x] register to site: `admin.site.register(Project, ProjectAdmin)`

**Test Feature 4**
- [x] `python manage.py test tests.test_feature_04`: OK
  
**Add and commit progress:**
  - [x] `git status`
  - [x] `git add .`
  - [x] `git status`
  - [x] `git commit -m "Feature 4 Complete`
  - [x] `git push origin main`

-----------------

### Feature 5 ###

>**Working in *projects* App: views.py, urls.py, & templates**

**Create a ListView for *Project* model**
- [x] Import model: `from projects.models import Project`
- [x] Import class view, ListView: `from django.views.generic.list import ListView`
- [x] Create a list view for Project model
```
class ProjectListView(ListView):
    model = Project
    template_name = "projects/list.html"
    context_object_name = "projectslist"
```

**Register the ProjectListView for a path**
- [x] Create a urls.py file in *projects* App
- [x] Import path: `from django.urls import path`
- [x] Import view: `from projects.views import ProjectListView`
- [x] Create path: urlpatterns = [`path(route="", view=ProjectListView.as_view(), name="list_projects")`,]

**Include URL patterns from *project* App in the *tracker* Project urls.py**
- [x] Import include: `from django.urls import path, include`
- [x] Create path: `path("projects/", include("projects.urls")),`

**Create template for ProjectListView**
- [x] Create a templates folder in *projects* App
- [x] Create a subfolder, projects, in template folder
- [x] Create base.html in templates folder and add below template:
```
{% load static %}

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BASE</title>
    <link rel="stylesheet" href="{% static 'css/app.css' %}">
</head>
<body>
    <header>
    </header>
    <main>
        {% block content %}
        {% endblock %}
    </main>
<body>


</html>
```

- [x] Create list.html in template/projects folder
    - [x] fundamental 5 (in base.html)
    - [x] main tag (in base.html)
      - [x] div tag
        - [x] h1 tag "My Projects"
        - [x] if no projects created: p tag: "You are not assigned to any projects"
        - [x] otherwise: table with two columns: table headers: Name, Number of tasks
``` 
{% extends "../base.html" %}
{% block content %}

<div>
    <h1>My Projects</h1>
    {% if not projectslist  %}
        <p>You are not assigned to any projects</p>
    {% else %}
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Number of tasks</th>
                </tr>
            </thead>
            <tbody>
                {% for object in projectslist %}
                <tr>
                    <td>{{ object.name }}</td>
                    {% comment %} <td>{{ object.total }}</td> {% endcomment %}
                </tr>
                {% endfor %}
    {% endif %}
            </tbody>
        </table>
</div>

{% endblock %}
```
**Test Feature 5**
- [x] `python manage.py test tests.test_feature_05`: OK
  
**Add and commit progress:**
  - [x] `git status`
  - [x] `git add .`
  - [x] `git status`
  - [x] `git commit -m "Feature 5 Complete`
  - [x] `git push origin main`

------------------

### Feature 6 ###

>**Working in *tracker* Project, urls.py**

**Redirect the default path**
- [x] Import RedirectView: `from django.views.generic.base import RedirectView`
- [x] Import reverse_lazy from Django.urls
- [x] Create path: `path("", RedirectView.as_view(url=reverse_lazy("list_projects")), name="home")`
  
**Test Feature 6**
- [x] `python manage.py test tests.test_feature_06`: OK
  
**Add and commit progress:**
  - [x] `git status`
  - [x] `git add .`
  - [x] `git status`
  - [x] `git commit -m "Feature 6 Complete`
  - [x] `git push origin main`

--------------

### Feature 7 & 9 ###
>**Working in *accounts* App, urls.py**

**Creating LoginView(& LogoutView) and add path**

- [x] Create a urls.py file in *accounts* App
- [x] Import path: `from django.urls import path`
- [x] Import: `from django.contrib.auth import views as auth_views`
- [x] Register the LoginView:
```
urlpatterns = [
    path('login/', auth_views.LoginView.as_view(), name="login"),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('signup/', SignupView, name="signup"),
]
```
**Include URL patterns from *accounts* App in the *tracker* Project urls.py**
- [x] Create path: `path("accounts/", include("accounts.urls")),`
- [x] Create a templates directory in *accounts* App
- [x] Create subdirectory *registration* in templates directory
- [x] Create login.html file in registration directory
- [x] In *tracker* Project, settings.py: create redirects for login and logout: 
`LOGIN_REDIRECT_URL = "home"`
`LOGOUT_REDIRECT_URL = "login"`

- [x] For login.html:
  - [x] fundamental 5
  - [x] main tag
  - [x] div tag
  - [x] h1 tag "Login"
  - [x] form tag with method "post"
  - [x] username, password, login
```
{% load static %}

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <main>
        <div>
                    <h1>Login</h1>
                    {% if form.errors %}
            <p>Your username and password didn't match. Please try again.</p>
            {% endif %}

            {% if next %}
                {% if user.is_authenticated %}
                <p>Your account doesn't have access to this page. To proceed,
                please login with an account that has access.</p>
                {% else %}
                <p>Please login to see this page.</p>
                {% endif %}
            {% endif %}

            <form method="post" action="{% url 'login' %}">
            {% csrf_token %}
            <table>
            <tr>
                <td>{{ form.username.label_tag }}</td>
                <td>{{ form.username }}</td>
            </tr>
            <tr>
                <td>{{ form.password.label_tag }}</td>
                <td>{{ form.password }}</td>
            </tr>
            </table>

            <button type="submit">Login</buttom>
            <input type="hidden" name="next" value="{{ next }}">
            </form>
        </div>
    </main>
</body>

</html>
```
**Test Feature 7**
- [x] `python manage.py test tests.test_feature_07`: OK
  
**Add and commit progress:**
  - [x] `git status`
  - [x] `git add .`
  - [x] `git status`
  - [x] `git commit -m "Feature 7 Complete`
  - [x] `git push origin main`

----------------
### Feature 8 
>**Working in *projects* App**

**Require login for ProjectListView**

- [x] Import: `from django.contrib.auth.mixins import LoginRequiredMixin`
- [x] Add `LoginRequiredMixin` to parameters
- [x] Add `login_url = "login"`

**Filter Project objects: members -> logged in user**
```
def get_queryset(self):
        return Project.objects.filter(members=self.request.user)
```
**Test Feature 8**
- [x] `python manage.py test tests.test_feature_08`: OK
  
**Add and commit progress:**
  - [x] `git status`
  - [x] `git add .`
  - [x] `git status`
  - [x] `git commit -m "Feature 8 Complete`
  - [x] `git push origin main`
  
------------------

### Feature 9 completed on 7

**Test Feature 9**
- [x] `python manage.py test tests.test_feature_09`: OK
  
**Add and commit progress:**
  - [x] `git status`
  - [x] `git add .`
  - [x] `git status`
  - [x] `git commit -m "Feature 9 Complete`
  - [x] `git push origin main`


------------------

## Feature 10
>**Working in *accounts* App**

**Create ==UserRegistrationForm== in *accounts* App, forms.py**
- [x] Create a forms.py in *accounts* App
- [x] Import: `from django.contrib.auth.models import User`
- [x] Import: `from django.contrib.auth.forms import UserCreationForm`

**Create ==SignupView== in *accounts* App, views.py**
- [x] Import: `from accounts.forms import UserRegistrationForm`
- [x] Import: `from django.contrib.auth import authenticate, login`
- [x] Import: `from django.contrib.auth.models import User`
- [x] Import: `redirect` from django shortcuts
- [x] Create the view:
```
def SignupView(request):
    if request.method == "GET":
        form = UserRegistrationForm()
        context = { "form": form}
        return render(request, "registration/signup.html", context)
    if request.method == "POST":
        form = UserRegistrationForm(data = request.POST)
        if form.is_valid():
            user = User.objects.create_user(username=request.POST['username'], password=request.POST['password1'])
            user = authenticate(username=user.username, password=request.POST['password1'])
            login(request, user)
            return redirect('home')
        else:
            form = UserRegistrationForm()
            context = { "form": form}
            return render(request, 'registration/signup.html', context)
```
**Create path for SignupView in *accounts* App urls.py**
- [x] Import: `from accounts.views import SignupView`
- [x] Create path: `path('signup/', SignupView, name="signup"),`


**Create the signup.html template**
- [x] Create the signup.html template in registration directory
- [x] Template specifications:
  - [x] fundamental 5
  - [x] main tag
  - [x] div tag
  - [x] h1 tag "Signup"
  - [x] form tag with "post" method
  - [x] username, password1, password2

```
{% load static %}

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <main>
        <div>
                    <h1>Signup</h1>
            <form method="post" action="{% url 'signup' %}">
            {% csrf_token %}
            <table>
            <tr>
                <td>{{ form.username.label_tag }}</td> 
                <td>{{ form.username }}Required. 150 characters or fewer. Letters, digits, and @/./+/-/_ only.</td> 
            </tr>
            <tr>
                <td>{{ form.password1.label_tag }}</td>
                <td>{{ form.password1 }}</td>    
            </tr>
            <tr>
                <td>{{ form.password2.label_tag }}</td>
                <td>{{ form.password2 }}Enter the same password as before, for verification</td>
            </tr>
            </table>

            <button type="submit">Signup</buttom>
            <input type="hidden" name="next" value="{{ next }}">
            </form>
        </div>
        <ul>   
            <li>Your password can't be too similar to your other personal information.</li>
            <li>Your password must contain at least 8 characters.</li>
            <li>Your password can't be a commonly used password.</li>
            <li>Your password can't be entirely numeric.</li>
        </ul>
    </main>
</body>

</html>
```

**Test Feature 10**
- [x] `python manage.py test tests.test_feature_10`: OK
  
**Add and commit progress:**
  - [x] `git status`
  - [x] `git add .`
  - [x] `git status`
  - [x] `git commit -m "Feature 10 Complete`
  - [x] `git push origin main`

-----------------

### Feature 11

>**Working in *task* App**

**Create *Task* model in *task* App, models.py**

- [x] Import: `from django.contrib.auth.models import User`
- [x] Import: `from projects.models import Project`
- [x] Import: `from django.utils import timezone`

**Task model attributes and methods:**
```
class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField(null=True, blank=True)
    due_date = models.DateTimeField(null=True, blank=True)
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(Project, related_name="tasks", on_delete=CASCADE)
    assignee = models.ForeignKey(User, null=True, related_name="tasks", on_delete=NULL)

    def __str__(self):
        return self.name
```
MIGRATE!!! (forgot until feature 13)
python manage.py makemigrations
python manage.py migrate

**Test Feature 11**
- [x] `python manage.py test tests.test_feature_11`: OK
  
**Add and commit progress:**
  - [x] `git status`
  - [x] `git add .`
  - [x] `git status`
  - [x] `git commit -m "Feature 11 Complete`
  - [x] `git push origin main`

------------------

### Feature 12

>**Working in *tasks* App, admin.py**

- [x] Register *Task* model in *tasks* App, admin.py
  - [x] import model: `from tasks.models import Task`
  - [x] create admin class: `class TaskAdmin(admin.ModelAdmin): pass`
  - [x] register to site: `admin.site.register(Task, TaskAdmin)`

**Test Feature 12**
- [x] `python manage.py test tests.test_feature_12`: OK
  
**Add and commit progress:**
  - [x] `git status`
  - [x] `git add .`
  - [x] `git status`
  - [x] `git commit -m "Feature 12 Complete`
  - [x] `git push origin main`


-----------------

### Feature 13

>**Working in *projects* App, views.py, urls.py**

**Creating a DetailView for Project model**
- [x] Import: `from django.views.generic.detail import DetailView`
- [x] Create DetailView:
```
class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"
    context_object_name = "projectsdetail"
```
**Register the ProjectDetailView for a path**
- [x] Import view: `from projects.views import ProjectDetailView`
- [x] Create path: `path(route="<int:pk>/", view=ProjectDetailView.as_view(), name="show_projects")`

**Create template for ProjectDetailView**
- [x] Create a detail.html file in templates -> projects directory
```
<div>
    <h1>{{ projectsdetail.name }}</h1>
    <p>{{ projectsdetail.description}}</p>
    <h2>Tasks</h2>
    {% if projectsdetail.tasks.all %}
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Assignee</th>
                    <th>Start date</th>
                    <th>Due date</th>
                    <th>Is completed</th>
                </tr>
            </thead>
            <tbody>
                {% for object in projectsdetail.tasks.all %}
                <tr>
                    <td>{{ object.name }}</td>
                    <td>{{ object.assignee }}</td>
                    <td>{{ object.start_date }}</td>
                    <td>{{ object.due_date }}</td>
                    <td>{{ object.is_completed|yesno }}</td>
                </tr>
                {% endfor %}
    {% else %}
        <p>This project has no tasks</p>
    {% endif %}

</div>
```
**Update list template to show number of tasks**
`<td>{{ object.tasks.count }}</td>`

**Update list template: link from project name to detail view for that project**
`<td><a href="{% url 'show_project' object.pk %}">{{ object.name }}</a></td>`

**Test Feature 13**
- [x] `python manage.py test tests.test_feature_13`: OK
  
**Add and commit progress:**
  - [x] `git status`
  - [x] `git add .`
  - [x] `git status`
  - [x] `git commit -m "Feature 13 Complete`
  - [x] `git push origin main`

---------------------

### Feature 14

>**Working in *projects* App, views.py, urls.py**

**Create a CreateView for Project model**
- [x] Import CreatView: `from django.views.generic.edit import CreateView, DeleteView, UpdateView`
- [x] Import: `from django.urls import reverse_lazy`
- [x] Create view with login required and redirect to detailview
```
class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/create.html"
    fields = ["name", "description", "members"]
    
    def get_success_url(self):
        return reverse_lazy("show_project", kwargs={"pk": self.object.pk})
```

**Register the ProjectDetailView for a path**
- [x] Import view: `from projects.views import ProjectCreateView`
- [x] Create path: `path(route="create/", view=ProjectCreateView.as_view(), name="create_project")`

**Create template for ProjectCreateView**
- [x] Create a create.html file in templates -> projects directory
```
{% extends "../base.html" %} 
{% block content %}

<div>
    <h1>Create a new project</h1>
    <form method="post">
        {% csrf_token %}
        {{ form.as_p }}
        <button class="button" >Create</button>
    </form>
</div>



{% endblock content %}
```

**Add link to Project list view to navigate to Create a new project**
`<p><a href="{% url 'create_project' %}">Create a new project</a></p>`

**Test Feature 14**
- [x] `python manage.py test tests.test_feature_14`: OK
  
**Add and commit progress:**
  - [x] `git status`
  - [x] `git add .`
  - [x] `git status`
  - [x] `git commit -m "Feature 14 Complete`
  - [x] `git push origin main`

--------------

### Feature 15

>**Working in *tasks* App, views.py, urls.py**

**Create a CreateView for Task model**
- [x] Import: `from tasks.models import Task`
- [x] Import: `from django.urls import reverse_lazy`
- [x] Import: `from django.views.generic.edit import CreateView`
- [x] Import: `from django.contrib.auth.mixins import LoginRequiredMixin`
```
class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/create.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]
    login_url = 'login'
```

**Register the TaskCreateView for a path**
- [x] Create a urls.py file in task App
- [x] Import: `from django.urls import path`
- [x] Import: `from tasks.views import TaskCreateView`
- [x] Create path:
```
urlpatterns = [
    path("create/", TaskCreateView.as_view(), name="create_task")
]
```

**Register tasks urls.py in tracker project urls.py**
- [x] `path("tasks/", include("tasks.urls")),`


**Create a template for TaskCreateView**
- [x] Create a templates directory
- [x] Create a tasks subdirectory in templates
- [x] Create a create.html file
- [x] Copy base.html template into tasks App templates directory
```
{% extends "../base.html" %}
{% block content %}


<div>
    <h1>Create a new task</h1>
    <form method="post">
        {% csrf_token %}
        {{ form.as_p }}
        <button class="button" >Create</button>
    </form>
</div>
            
{% endblock %}
```

**Add link to create a task in the Project detail page**
`<p><a href="{% url 'create_task' %}">Create a new task</a></p>`

**Test Feature 15**
- [x] `python manage.py test tests.test_feature_15`: OK
  
**Add and commit progress:**
  - [x] `git status`
  - [x] `git add .`
  - [x] `git status`
  - [x] `git commit -m "Feature 15 Complete`
  - [x] `git push origin main`

-----------------

### Feature 16

>**Working in *tasks* App, views.py**

**Creating a TaskListView in task App, views.py**
- [x] Import: `from django.views.generic.list import ListView`
```
class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"
    context_object_name = "taskslist"
    login_url = 'login'

    def get_queryset(self):
        return Project.objects.filter(assignee=self.request.user)
```

**Register the TaskListView in task App urls.py**
- [x] Import the view, TaskListView
- [x] Create path: `path("mine/", TaskListView.as_view(), name="show_my_tasks")`


**Create list.html page with specifications**
- [x] Create list.html in tasks subdirectory in templates task App
```
{% extends "../base.html" %}
{% block content %}

<div>
    <h1>My Tasks</h1>
    {% if not taskslist %}
        <p>You have no tasks</p>
    {% else %}
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Start date</th>
                    <th>End date</th>
                    <th>Is completed</th>
                </tr>
            </thead>
            <tbody>
                {% for object in taskslist %}
                <tr>
                    <td>{{ object.name }}</td>
                    <td>{{ object.start_date }}</td>
                    <td>{{ object.due_date }}</td>
                    <td>{% if object.is_completed == False %}{% else %}Done{% endif %}</td>
                </tr>
                {% endfor %}
    {% endif %}
            </tbody>
        </table>
</div>

{% endblock %}
```

**Test Feature 16**
- [x] `python manage.py test tests.test_feature_16`: OK
  
**Add and commit progress:**
  - [x] `git status`
  - [x] `git add .`
  - [x] `git status`
  - [x] `git commit -m "Feature 16 Complete`
  - [x] `git push origin main`

-------------

### Feature 17

**Create an UpdateView for Task model**
- [x] Import: `from django.views.generic.edit import UpdateView`
- [x] Create TaskUpdateView
```
class TaskUpdateView(UpdateView):
    model = Task
    template_name = "tasks/update.html"
    fields = ["is_completed"]
    sucess_url = reverse_lazy("show_my_tasks")
```

**Register UpdateView in tasks App urls.py**
- [x] Import UpdateView
- [x] Path: ` path("<int:pk>/complete/", TaskUpdateView.as_view(), name="complete_task")`

*No need to make a template for this UpdateView*

**Modify TaskListView template with specifications:**
```
<td>{% if object.is_completed == False %}
    <form method="post" action="{% url 'complete_task' object.id %}">
    {% csrf_token %}
    <input type="hidden" name="is_completed" value="True">
    <button>Complete</button>
    </form>
    {% else %}Done{% endif %}</td>
```
**Test Feature 17**
- [x] `python manage.py test tests.test_feature_17`: OK
  
**Add and commit progress:**
  - [x] `git status`
  - [x] `git add .`
  - [x] `git status`
  - [x] `git commit -m "Feature 17 Complete`
  - [x] `git push origin main`

------------------

### Feature 18

- [x] pip install django-markdownify
- [x] add markdownify to INSTALLED_APPS in tracker Project:
`'markdownify.apps.MarkdownifyConfig',`
- [x] Add code below to Settings.py of tracker Project to disable sanitation:
```
MARKDOWNIFY = {
    "default": {
        "BLEACH": False
    }
}
```
**In Project DetailView template:**
`<p>{{ projectsdetail.description|markdownify }}</p>`

- [x] Update requirements.txt: `pip freeze > requirements.txt`

**Test Feature 18**
- [x] `python manage.py test tests.test_feature_18`: OK
  
**Add and commit progress:**
  - [x] `git status`
  - [x] `git add .`
  - [x] `git status`
  - [x] `git commit -m "Feature 18 Complete`
  - [x] `git push origin main`

-------------

### Feature 19

**Update the base.html files in each App to show desired links**
```

{% load static %}

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BASE</title>
    <link rel="stylesheet" href="{% static 'css/app.css' %}">
</head>
<body>
    <header>
        <nav>
            <ul>
                {% if user.is_authenticated %} 
                    <li><a href="{% url 'show_my_tasks' %}">My tasks</a></li>
                    <li><a href="{% url 'list_projects' %}">My projects</a></li>
                    <li><a href="{% url 'logout' %}">logout</a></li>
                {% endif %}
                {% if not user.is_authenticated %}
                    <li><a href="{% url 'login' %}">Signup</a></li>
                    <li><a href="{% url 'signup' %}">Login</a></li>
                {% endif %}
            </ul>
        </nav> 
    </header>
    <main>
        {% block content %}
        {% endblock %}
    </main>
<body>


</html>
```

**Test Feature 19**
- [x] `python manage.py test tests.test_feature_19`: OK
  
**Add and commit progress:**
  - [x] `git status`
  - [x] `git add .`
  - [x] `git status`
  - [x] `git commit -m "Feature 19 Complete`
  - [x] `git push origin main`






